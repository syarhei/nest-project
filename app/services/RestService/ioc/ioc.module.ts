import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { UserModule } from "../../UserService/ioc/ioc.module";

@Module({
    imports: [ UserModule ],
    controllers: [],
    providers: []
})
export class MainModule implements NestModule {
    public configure (consumer: MiddlewareConsumer): MiddlewareConsumer {
        return consumer;
    }
}