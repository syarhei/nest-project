
export interface IUserPresentation {
    id: string;
    name: string;
}