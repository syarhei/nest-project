import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("user")
export class UserEntity {
    @PrimaryGeneratedColumn("uuid")
    public id: string;
    @Column("varchar", { length: 30, unique: true, nullable: false })
    public name: string;
}