import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserEntity } from "../models/data/UserEntity";
import { Repository } from "typeorm";
import { UserMapper } from "../mappers/UserMapper";
import { IUser } from "../models/contracts/IUser";

@Injectable()
export class UserRepository {
    constructor (
        @InjectRepository(UserEntity) private user: Repository<UserEntity>
    ) {}

    public async getUsers (): Promise<IUser[]> {
        const users: UserEntity[] = await this.user.find();
        return users.map(user => UserMapper.userEntityToUser(user));
    }
}