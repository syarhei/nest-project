import { IUserService } from "./IUserService";
import { IUser } from "./models/contracts/IUser";
import { Inject, Injectable } from "@nestjs/common";
import { USER_REPOSITORY } from "./ioc/ioc.ids";
import { UserRepository } from "./DAL/UserRepository";

@Injectable()
export class UserService implements IUserService {
    constructor (
        @Inject(USER_REPOSITORY) private userRepository: UserRepository
    ) {}

    public async getUsers (): Promise<IUser[]> {
        return this.userRepository.getUsers();
    }
}