import { IUser } from "./models/contracts/IUser";

export interface IUserService {
    getUsers (): Promise<IUser[]>;
}