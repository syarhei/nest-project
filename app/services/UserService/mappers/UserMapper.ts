import { UserEntity } from "../models/data/UserEntity";
import { IUser } from "../models/contracts/IUser";

export class UserMapper {
    public static userEntityToUser (userEntity: UserEntity): IUser {
        return {
            id: userEntity.id,
            name: userEntity.name
        };
    }
}