import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserService } from "../UserService";
import { USER_REPOSITORY, USER_SERVICE } from "./ioc.ids";
import { UserRepository } from "../DAL/UserRepository";
import dbConfig from "../../../db/config";
import { UserEntity } from "../models/data/UserEntity";

@Module({
    imports: [ TypeOrmModule.forRoot(dbConfig), TypeOrmModule.forFeature([ UserEntity ]) ],
    providers: [
        { useClass: UserService, provide: USER_SERVICE },
        { useClass: UserRepository, provide: USER_REPOSITORY },
    ],
    exports: [
        { useClass: UserService, provide: USER_SERVICE }
    ]
})
export class UserModule {}