import { NestFactory } from "@nestjs/core";
import { MainModule } from "./services/RestService/ioc/ioc.module";
import { INestApplication } from "@nestjs/common";

let application: INestApplication = null;

async function initializeServer (): Promise<void> {
    application = await NestFactory.create(MainModule);
}

async function startServer (): Promise<void> {
    await application.listen(3000);
    console.log("Server is starting");
}

async function main (): Promise<void> {
    await initializeServer();
    await startServer();
}

(async () => {
    try {
        await main();
    } catch (err) {
        console.error(err);
    }
})();