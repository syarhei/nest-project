import { UserEntity } from "../services/UserService/models/data/UserEntity";
import { SqlServerConnectionOptions } from "typeorm/driver/sqlserver/SqlServerConnectionOptions";

const dbConfig: SqlServerConnectionOptions = {
    type: "mssql",
    host: "localhost\\SQLEXPRESS",
    username: "sa",
    password: "admin123",
    database: "altria-local",
    entities: [ UserEntity ],
    synchronize: true,
    options: {
        encrypt: true
    }
};

export default dbConfig;